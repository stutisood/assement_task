const jwt = require('jsonwebtoken')
const userData = require('../database/userData')
const createPost = require('../database/createPost')
const secretKey = "secretKey";
const multer = require('multer');

// secret key for JWT
const secret = secretKey;

// multer for file upload
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const authenticateToken = (req, res, next) => {
  const token = req.header('Authorization');
  if (!token) return res.status(401).send('Access denied');

  jwt.verify(token, secret, (err, user) => {
    if (err) return res.status(403).send('Invalid token');
    req.user = user;
    next();
  });
};


module.exports.login = async(req,res) => {
    const record = await userData.create({
        username: req.body.username,
        password: req.body.password
    })

    const token = jwt.sign({ userId: record._id, username: record.username }, secretKey);

    record.token = token;
    await record.save();

    return res.json({record, token})
}


module.exports.sendFile = (authenticateToken, upload.single('user_file'), (req, res) => {
  try {
    const record = req.header;
    const file = req.file;
    
    return res.json({
        msg : "file uploaded",
         record, file });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});


module.exports.createPost = (authenticateToken, async(req, res) => {
    const post = await createPost.create({
        userId : req.params,
        title: req.body.title,
        content: req.body.content,
    })

    return res.json({post})
})


module.exports.updatePost = (authenticateToken, async (req, res) => {
    try {
      const { postId } = req.params;
      const post = await createPost.findOneAndUpdate({ _id: postId, userId},{
         title : req.body.title,
         content : req.body.content
      });
     res.json(post);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  });


  module.exports.aggreagetPost = (authenticateToken, async (req, res) => {
    try {
      const { postId } = req.params;
      const user = req.record;
  
      const likes = await createPost.aggregate([
        { $match: { _id: mongoose.Types.ObjectId(postId), user: user._id } },
        {
          $lookup: {
            from: 'UserData',
            localField: 'likes',
            foreignField: '_id',
            as: 'likedUsers',
          },
        },
        { $project: { likedUsers: 1, _id: 0 } },
      ]);
  
      res.json(likes);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
  });
  