const mongoose = require('mongoose');

const likeSchema = new mongoose.Schema({
    postId: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'createPost'
    },
    postName: {
        type: String
    }
  });

const like =  mongoose.model('likes', likeSchema);
module.exports = like
  