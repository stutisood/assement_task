const mongoose = require('mongoose');

const createPostSchema = new mongoose.Schema({

    userId: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'userData'
    },

    title: { type: String, required: true},
    content: { type: String, required: true },
  });
  
  const createPost =  mongoose.model('createPost', createPostSchema);
  module.exports = createPost