const mongoose = require('mongoose');


mongoose.connect("mongodb://127.0.0.1:27017/task")
.then(function(db){
    console.log("database connected")
})
.catch(function(err){
    console.log(err,"Some Error")
})

const userSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  token: { type: String },
});

const User =  mongoose.model('User', userSchema);
module.exports = User