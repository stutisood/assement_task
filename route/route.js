const express = require('express')
const controller = require("../controller/apiMethods")
const router = express.Router();

router.post('/login', controller.login)
router.post('/createPost/:_id', controller.createPost)
router.post('/sendFile', controller.sendFile)
router.put('./updatePost/:_id', controller.updatePost)
//router.get('./aggreagatePost/:postId', controller.aggreagetPost)

module.exports=router